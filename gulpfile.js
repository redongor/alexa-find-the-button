const gulp = require('gulp');
const clean = require('gulp-clean');
const merge = require('merge-stream');
const notify = require('gulp-notify');
const shell = require('gulp-shell');
const ts = require('gulp-typescript');
const zip = require('gulp-zip');

const tsProject = ts.createProject('tsconfig.json');

const SKILL_NAME = 'find-the-button_update';
const PACKAGE_FILENAME = 'build.zip';

gulp.task('clean', cleanTask);
gulp.task('build', gulp.series(['clean'], buildTask));
gulp.task('zip', gulp.series(['build'], zipTask));
gulp.task('deploy:skill', deploySkill);
gulp.task('deploy:model', deployModel);
gulp.task('deploy:lambda:us', gulp.series(['zip'], () => deployLambda('us-east-1')))
gulp.task('deploy:lambda:eu', gulp.series(['zip'], () => deployLambda('eu-west-1')))
gulp.task('deploy:lambda:ap', gulp.series(['zip'], () => deployLambda('ap-northeast-1')))
gulp.task('deploy:lambda', gulp.series(['deploy:lambda:us', 'deploy:lambda:eu', 'deploy:lambda:ap']));
gulp.task('deploy', gulp.series(['deploy:skill', 'deploy:model', 'deploy:lambda']));

function deploySkill() {
    return gulp.src('')
        .pipe(shell(`ask deploy --force --target skill`))
        .pipe(notify({
            message: `✓ deployed skill`,
            sound: 'Glass'
        }));
}

function deployModel() {
    return gulp.src('')
        .pipe(shell(`ask deploy --force --target model`))
        .pipe(notify({
            message: `✓ deployed model`,
            sound: 'Glass'
        }));
}

function deployLambda(region) {
    return gulp.src('./')
        .pipe(shell(`aws lambda update-function-code --region ${region} --function-name alexa-${SKILL_NAME} --zip-file fileb://./${PACKAGE_FILENAME}`))
        .pipe(notify({
            message: `✓ deployed lambda to ${region}`,
            sound: 'Glass'
        }));
}

function zipTask() {
    return gulp.src(['./build/**/*.*'], { buffer: true })
        .pipe(zip(PACKAGE_FILENAME))
        .pipe(gulp.dest('./'));
}

function buildTask() {
    const appFiles = gulp.src('./lambda/custom/**/*.ts')
        .pipe(tsProject())
        .pipe(gulp.dest('./build'));

    const nodeFiles = gulp.src('./node_modules/**/*.*', { base: './' })
        .pipe(gulp.dest('./build'));

    return merge(appFiles, nodeFiles);
}

function cleanTask() {
    return gulp.src(['build/', PACKAGE_FILENAME], { read: false, allowEmpty: true })
        .pipe(clean());
}
