import { DynamoDB } from 'aws-sdk';
import { User } from '../data-models/User';
const docClient = new DynamoDB.DocumentClient();

const TABLE_NAME = 'alexa-users';

export const ddb = {
    getUser: (userId: string, cb: Function) => {
        console.log('getUser(): ', userId);
        const params = {
            TableName: TABLE_NAME,
            Key: {
                'userId': userId
            }
        };

        console.log('getting user with id: ', userId);

        docClient.get(params, function(err, response) {
            if (err) {
                console.log('error in getUser: ', err);
                throw err;
            }
            cb(response.Item);
        });
    },
    updateUser: (user: User, cb: Function) => {
        const params = {
            TableName: TABLE_NAME,
            Key: {
                'userId': user.userId
            },
            AttributeUpdates: {
                'apps': {
                    Action: 'PUT',
                    Value: user.apps
                }
            }
        };

        console.log('updating user: ', user);

        docClient.update(params, (err, response) => {
            if (err) {
                console.error('error in updateUser: ', err);
                throw err;
            }
            cb(response);
        });
    }
};
