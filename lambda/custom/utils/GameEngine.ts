import { Directive } from "ask-sdk-model";

type EventRequest = {
    type: string;
    originatingRequestId: string;
    events: [{name: string}]
}

export class GameEngine {
    public static startInputHandler(timeout: number = 30, buttonId?: string): Directive {
        return {
            type: 'GameEngine.StartInputHandler',
            timeout: timeout * 1000,
            proxies: buttonId ? undefined : ['button1'],
            recognizers: {
                'buttonUpRecognizer': this.createButtonUpRecognizer(buttonId ? buttonId : undefined),
                'buttonDownRecognizer': this.createButtonDownRecognizer(buttonId ? buttonId : undefined)
            },
            events: {
                'buttonDown': this.createButtonDownEvent(),
                'buttonUp': this.createButtonUpEvent(),
                'timedOut': this.createTimeOutEvent()
            }
        } as Directive;
    }

    public static createStopInputHandlerDirective(requestId: string): Directive {
        return {
            type: 'GameEngine.StopInputHandler',
            originatingRequestId: requestId
        };
    }

    public static matchesEvent(request: EventRequest, handlerId: string, eventName: string): boolean {
        let doesMatch = false;

        if (request.events) {
            for (let i = 0; i < request.events.length && !doesMatch; i++) {
                console.log(`-- Testing for Event Match: ${JSON.stringify({
                    type: request.type,
                    handlerId: handlerId || 'no handler id???',
                    event: request.events[i],
                    eventName: eventName,
                    originatingRequestId: request.originatingRequestId
                }, null, 4)}`);
                doesMatch = request.type === 'GameEngine.InputHandlerEvent'
                    && handlerId === request.originatingRequestId
                    && request.events[i].name === eventName;
            }
        }

        return doesMatch;
    }

    private static createButtonUpRecognizer(buttonId?: string) {
        return {
            type: 'match',
            anchor: 'anywhere',
            gadgetIds: buttonId ? [buttonId] : ['button1'],
            pattern: [
                { action: 'up' }
            ]
        };
    }

    private static createButtonUpEvent() {
        return {
            meets: ['buttonUpRecognizer'],
            reports: 'matches',
            shouldEndInputHandler: true,
            maximumInvocations: 1
        };
    }

    private static createButtonDownRecognizer(buttonId?: string) {
        return {
            type: 'match',
            anchor: 'anywhere',
            gadgetIds: buttonId ? [buttonId] : ['button1'],
            pattern: [
                { action: 'down' }
            ]
        };
    }

    private static createButtonDownEvent() {
        return {
            meets: ['buttonDownRecognizer'],
            reports: 'matches',
            shouldEndInputHandler: false
        };
    }

    private static createTimeOutEvent() {
        return {
            meets: ['timed out'],
            shouldEndInputHandler: true
        };
    }
}
