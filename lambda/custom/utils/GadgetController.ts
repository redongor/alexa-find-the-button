import { interfaces } from "ask-sdk-model";

import SetLightDirective = interfaces.gadgetController.SetLightDirective;

/* todo enhancements
    * access session attributes here, like flashingdisabled
    * store button animation state and stuff in session attributes
    ** this will enable creating dynamic animations without passing in a bunch of args
    **-like the button down animation
*/

type SequenceStep = {
    durationMs: number,
    color: string,
    blend: boolean
}

type Animation = {
    repeat?: number,
    targetLights?: string[],
    sequence: SequenceStep[]
};

type SetLightOptions = {
    animations: Animation[],
    targetGadgets?: [string],
    triggerEvent?: string,
    triggerEventTimeMs?: number
};

export class GadgetController {
    public static stopDefaultAnimation() {
        return this.createSetLightDirective({
            animations: [{
                repeat: 1,
                sequence: [
                    {
                        durationMs: 1,
                        color: '000000',
                        blend: false
                    }
                ]
            }]
        });
    }

    public static stopButtonDownAnimation() {
        return this.createSetLightDirective({
            triggerEvent: 'buttonDown',
            animations: [{
                repeat: 1,
                sequence: [
                    {
                        durationMs: 1,
                        color: '000000',
                        blend: false
                    }
                ]
            }]
        });
    }

    public static stopButtonUpAnimation() {
        return this.createSetLightDirective({
            triggerEvent: 'buttonUp',
            animations: [{
                repeat: 1,
                sequence: [
                    {
                        durationMs: 1,
                        color: '000000',
                        blend: false
                    }
                ]
            }]
        });
    }

    public static createRollCallAnimation(): SetLightDirective {
        return this.createSetLightDirective({
            animations: [
                {
                    repeat: 255,
                    sequence: [
                        {
                            durationMs: 2000,
                            color: '0000FF',
                            blend: true
                        },
                        {
                            durationMs: 2000,
                            color: '000000',
                            blend: true
                        },
                    ]
                }
            ]
        });
    }

    public static createHelpAnimation(buttonId?: string): SetLightDirective {
        return this.createSetLightDirective({
            targetGadgets: buttonId ? [ buttonId ] : undefined,
            animations: [
                {
                    repeat: 1,
                    sequence: [
                        {
                            durationMs: 30000,
                            color: 'FFA500',
                            // color: '000000',
                            blend: false
                        }
                    ]
                }
            ]
        });
    }

    public static createButtonNotFoundAnimation(buttonId?: string): SetLightDirective {
        return this.createSetLightDirective({
            targetGadgets: buttonId ? [ buttonId ] : undefined,
            animations: [
                {
                    repeat: 1,
                    sequence: [
                        {
                            durationMs: 30000,
                            color: 'FF0000',
                            blend: false
                        }
                    ]
                }
            ]
        });
    }

    public static createHideButtonAnimation(buttonId: string, triggerTime?: number): SetLightDirective {
        return this.createSetLightDirective({
            targetGadgets: [ buttonId ],
            triggerEventTimeMs: triggerTime || 0,
            animations: [{
                repeat: 1,
                sequence: [
                        {
                            durationMs: 15000,
                            color: '00FF00',
                            blend: false
                        },
                        {
                            durationMs: 5000,
                            color: 'AAAA00',
                            blend: false
                        },
                        {
                            durationMs: 5000,
                            color: 'FF5500',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                        // extension beyond 30 seconds
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: '000000',
                            blend: false
                        },
                        {
                            durationMs: 500,
                            color: 'FF0000',
                            blend: false
                        },
                    ]
            }]
        });
    }

    public static createButtonUpAnimation(buttonId?: string,
                                            color1?: string,
                                            color2?: string) {
        return this.createSetLightDirective({
            targetGadgets: buttonId ? [ buttonId ] : undefined,
            triggerEvent: 'buttonUp',
            animations: [{
                repeat: 2,
                sequence: [
                    {
                        durationMs: 100,
                        color: color1 || '00FF00',
                        blend: false
                    },
                    {
                        durationMs: 100,
                        color: color2 || '000000',
                        blend: false
                    }
                ]
            }]
        })
    }

    public static createButtonDownAnimation(buttonId?: string,
                                            color1?: string,
                                            color2?: string): SetLightDirective {
        return this.createSetLightDirective({
            targetGadgets: buttonId ? [ buttonId ] : undefined,
            triggerEvent: 'buttonDown',
            animations: [{
                repeat: 1,
                sequence: [
                    {
                        durationMs: 100,
                        color: color1 || '00FF00',
                        blend: false
                    },
                    {
                        durationMs: 100,
                        color: color2 || '000000',
                        blend: false
                    }
                ]
            }]
        });
    }

    public static createFindButtonAnimation(buttonId: string): SetLightDirective {
        return this.createSetLightDirective({
            targetGadgets: [ buttonId ],
            triggerEventTimeMs: 3000,
            animations: [{
                repeat: 1,
                sequence: [
                    {
                        durationMs: 10000,
                        color: '000000',
                        blend: false
                    },
                    {
                        durationMs: 300,
                        color: '0000FF',
                        blend: false
                    },
                    {
                        durationMs: 1700,
                        color: '000000',
                        blend: false
                    },
                    {
                        durationMs: 300,
                        color: '0000FF',
                        blend: false
                    },
                    {
                        durationMs: 1700,
                        color: '000000',
                        blend: false
                    },
                    {
                        durationMs: 300,
                        color: '0000FF',
                        blend: false
                    },
                    {
                        durationMs: 1700,
                        color: '000000',
                        blend: false
                    },
                    {
                        durationMs: 300,
                        color: '0000FF',
                        blend: false
                    },
                    {
                        durationMs: 1700,
                        color: '000000',
                        blend: false
                    },
                    {
                        durationMs: 300,
                        color: '0000FF',
                        blend: false
                    },
                    {
                        durationMs: 1700,
                        color: '000000',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FF0000',
                        blend: false
                    },
                    {
                        durationMs: 1500,
                        color: '000000',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FF0000',
                        blend: false
                    },
                    {
                        durationMs: 1500,
                        color: '000000',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FFA500',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FF0000',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FFA500',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FF0000',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FFA500',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FF0000',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FFA500',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FF0000',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FFA500',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FF0000',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FFA500',
                        blend: false
                    },
                    {
                        durationMs: 500,
                        color: 'FF0000',
                        blend: false
                    }
                ]
            }]
        });
    }

    public static createGameOverAnimation(buttonId: string): SetLightDirective {
        return this.createSetLightDirective({
            targetGadgets: [ buttonId ],
            animations: [
                {
                    repeat: 255,
                    sequence: [
                        {
                            durationMs: 2000,
                            color: '0000FF',
                            blend: true
                        },
                        {
                            durationMs: 2000,
                            color: '000000',
                            blend: true
                        },
                    ]
                }
            ]
        });
    }

    private static createSetLightDirective(options: SetLightOptions): SetLightDirective {
        return {
            type: 'GadgetController.SetLight',
            version: 1,
            targetGadgets: options.targetGadgets || undefined,
            parameters: {
                triggerEvent: options.triggerEvent || 'none',
                triggerEventTimeMs: options.triggerEventTimeMs || 0,
                animations: this.buildAnimation(options.animations)
            }
        } as SetLightDirective;
    }

    private static buildAnimation(animations: Animation[]): Animation[] {
        const animation = animations[0];
        return [{
            repeat: animation.repeat || 1, //maybe this should be 0? not sure
            targetLights: ['1'],
            sequence: animation.sequence
        }];
    }
}
