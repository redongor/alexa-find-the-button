import { NoIntentHandler } from "../classes/NoIntentHandler.class";
import { STATES } from "../../data/states";
import { HandlerInput } from "ask-sdk";

export class RollCallNoHandler extends NoIntentHandler {
    constructor() {
        super(STATES.RollCall);
    }

    public canHandle(handlerInput: HandlerInput) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        return super.canHandle(handlerInput)
            && (sessionAttributes.hasBeenAskedAQuestion || sessionAttributes.hasTimedOut);
    }
}
