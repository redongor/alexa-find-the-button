import { InputEventHandler } from "../classes/InputEventHandler.class";
import { STATES } from "../../data/states";
import { ResponseBuilder, HandlerInput } from "ask-sdk";
import { GameEngine, GadgetController } from "../../utils";

export class ButtonRegisteredEventHandler extends InputEventHandler {
    constructor() {
        super('buttonUp', STATES.RollCall, STATES.HideButton);
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.setSessionAttribute('buttonId', (handlerInput.requestEnvelope.request as any).events[0].inputEvents[0].gadgetId);
        this.updateHandlerId();
        this.setSessionAttribute('handlerStartTime', Date.now());
        super.updateSessionAttributes(handlerInput);
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const buttonId = this.getSessionAttribute('buttonId');
        const flashingDisabled = this.getSessionAttribute('flashingDisabled');

        let response = super.createResponse(handlerInput)
            .addDirective(GameEngine.startInputHandler(40, buttonId))
            .addDirective(GadgetController.stopButtonDownAnimation())
            .addDirective(GadgetController.stopButtonUpAnimation())
            .addDirective(GadgetController.stopDefaultAnimation());

        if (!flashingDisabled) {
            response = response
                .addDirective(GadgetController.createButtonDownAnimation(buttonId))
                .addDirective(GadgetController.createButtonUpAnimation(buttonId))
                .addDirective(GadgetController.createHideButtonAnimation(buttonId, 10000));
        }

        return response;
    }
}
