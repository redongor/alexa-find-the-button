import { YesIntentHandler } from "../classes/YesIntentHandler.class";
import { STATES } from "../../data/states";
import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { GameEngine, GadgetController } from "../../utils";

export class RollCallYesHandler extends YesIntentHandler {
    constructor() {
        super(STATES.RollCall);
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.setSessionAttribute('inputHandlerId', handlerInput.requestEnvelope.request.requestId);
        return super.updateSessionAttributes(handlerInput);
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const flashingDisabled = this.getSessionAttribute('flashingDisabled');

        let response = super.createResponse(handlerInput)
            .addDirective(GameEngine.startInputHandler(30, this.getSessionAttribute('buttonId')))
            .addDirective(GadgetController.stopButtonDownAnimation())
            .addDirective(GadgetController.stopButtonUpAnimation())
            .addDirective(GadgetController.stopDefaultAnimation());

        if (!flashingDisabled) {
            response = response
                .addDirective(GadgetController.createButtonDownAnimation())
                .addDirective(GadgetController.createButtonUpAnimation())
                .addDirective(GadgetController.createRollCallAnimation());
        }

        return response;
    }
}
