import { ButtonRegisteredEventHandler } from './ButtonRegisteredEventHandler';
import { RollCallYesHandler } from './RollCallYesHandler';
import { RollCallNoHandler } from './RollCallNoHandler';
import { RollCallTimedOutHandler } from './RollCallTimedOutHandler';
import { RollCallHelpHandler } from './RollCallHelpHandler';

export const RollCallStateRequestHandlers = [
    new ButtonRegisteredEventHandler(),
    new RollCallHelpHandler(),
    new RollCallYesHandler(),
    new RollCallNoHandler(),
    new RollCallTimedOutHandler()
];
