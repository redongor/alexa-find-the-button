import { InputEventHandler } from "../classes/InputEventHandler.class";
import { STATES } from "../../data/states";
import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { GadgetController } from "../../utils";

export class RollCallTimedOutHandler extends InputEventHandler {
    constructor() {
        super('timedOut', STATES.RollCall);
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.setSessionAttribute('hasTimedOut', true);
        this.setSessionAttribute('hasBeenAskedAQuestion', true);
        this.removeSessionAttribute('inputHandlerId');
        this.saveSessionAttributes();
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const flashingDisabled = this.getSessionAttribute('flashingDisabled');

        let response = super.createResponse(handlerInput)
            .addDirective(GadgetController.stopButtonDownAnimation())
            .addDirective(GadgetController.stopButtonUpAnimation())
            .addDirective(GadgetController.stopDefaultAnimation())
            .withShouldEndSession(false);

        if (!flashingDisabled) {
            response = response
                .addDirective(GadgetController.createButtonDownAnimation(undefined, 'FF0000'))
                .addDirective(GadgetController.createButtonUpAnimation(undefined, 'FF0000'))
                .addDirective(GadgetController.createHelpAnimation(this.getSessionAttribute('buttonId')));
        }

        return response;
    }
}
