import { HelpIntentHandler } from "../classes/HelpIntentHandler.class";
import { STATES } from "../../data/states";

export class RollCallHelpHandler extends HelpIntentHandler {
    constructor() {
        super(STATES.RollCall);
    }
}
