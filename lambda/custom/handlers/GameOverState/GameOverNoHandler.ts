import { STATES } from "../../data/states";
import { NoIntentHandler } from "../classes/NoIntentHandler.class";

export class GameOverNoHandler extends NoIntentHandler {
    constructor() {
        super(STATES.GameOver);
    }
}
