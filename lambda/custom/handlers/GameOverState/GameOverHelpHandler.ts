import { HelpIntentHandler } from "../classes/HelpIntentHandler.class";
import { STATES } from "../../data/states";

export class GameOverHelpHandler extends HelpIntentHandler {
    constructor() {
        super(STATES.GameOver);
    }
}