import { InputEventHandler } from "../classes/InputEventHandler.class";
import { STATES } from "../../data/states";
import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { GadgetController } from "../../utils";
import { Response } from "ask-sdk-model";

export class GameOverTimedOutEventHandler extends InputEventHandler {
    constructor() {
        super('timedOut', STATES.GameOver);
    }

    public handle(handlerInput: HandlerInput): Response {
        this.initializeAttributeHandling(handlerInput);
        const hasBeenAskedAQuestion = this.getSessionAttribute('hasBeenAskedAQuestion');
        if (hasBeenAskedAQuestion) {
            return handlerInput.responseBuilder.getResponse();
        }
        else {
            return super.handle(handlerInput);
        }
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.setSessionAttribute('hasTimedOut', true);
        this.setSessionAttribute('hasBeenAskedAQuestion', true);
        this.removeSessionAttribute('inputHandlerId');
        this.removeSessionAttribute('handlerStartTime');
        this.saveSessionAttributes();
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const buttonId = this.getSessionAttribute('buttonId');
        const flashingDisabled = this.getSessionAttribute('flashingDisabled');

        let response = super.createResponse(handlerInput)
            .addDirective(GadgetController.stopButtonDownAnimation())
            .addDirective(GadgetController.stopButtonUpAnimation())
            .addDirective(GadgetController.stopDefaultAnimation())
            .withShouldEndSession(false);

        if (!flashingDisabled) {
            response = response
                .addDirective(GadgetController.createButtonDownAnimation(buttonId, 'FF0000'))
                .addDirective(GadgetController.createButtonUpAnimation(buttonId, 'FF0000'))
                .addDirective(GadgetController.createHelpAnimation(buttonId))
        }

        return response;
    }
}
