import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { STATES } from "../../data/states";
import { GadgetController, GameEngine } from "../../utils";
import { YesIntentHandler } from "../classes/YesIntentHandler.class";

export class GameOverYesHandler extends YesIntentHandler {
    constructor() {
        super(STATES.GameOver);
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.setSessionAttribute('inputHandlerId', handlerInput.requestEnvelope.request.requestId);
        this.setSessionAttribute('handlerStartTime', Date.now());
        super.updateSessionAttributes(handlerInput);
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const buttonId = this.getSessionAttribute('buttonId');
        const flashingDisabled = this.getSessionAttribute('flashingDisabled');

        let response = super.createResponse(handlerInput)
            .addDirective(GameEngine.startInputHandler(70))
            .addDirective(GadgetController.stopDefaultAnimation())
            .addDirective(GadgetController.stopButtonDownAnimation())
            .addDirective(GadgetController.stopButtonUpAnimation());

        if (!flashingDisabled) {
            response = response
                .addDirective(GadgetController.createButtonDownAnimation(buttonId))
                .addDirective(GadgetController.createButtonUpAnimation(buttonId))
                .addDirective(GadgetController.createGameOverAnimation(buttonId));
        }

        return response;
    }
}
