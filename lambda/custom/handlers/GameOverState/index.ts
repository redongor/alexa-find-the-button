import { GameOverYesHandler } from "./GameOverYesHandler";
import { GameOverNoHandler } from "./GameOverNoHandler";
import { GameOverHelpHandler } from "./GameOverHelpHandler";
import { RestartHandler } from "./RestartHandler";
import { GameOverTimedOutEventHandler } from "./GameOverTimedOutEventHandler";

export const GameOverStateRequestHandlers = [
    new GameOverHelpHandler(),
    new GameOverYesHandler(),
    new GameOverNoHandler(),
    new GameOverTimedOutEventHandler(),
    new RestartHandler()
];
