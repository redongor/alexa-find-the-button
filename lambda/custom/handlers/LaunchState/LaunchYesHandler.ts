import { YesIntentHandler } from "../classes/YesIntentHandler.class";
import { STATES } from "../../data/states";
import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { GadgetController } from "../../utils";

export class LaunchYesHandler extends YesIntentHandler {
    constructor() {
        super(STATES.Launch);
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.setSessionAttribute('hasBeenAskedAQuestion', true);
        this.saveSessionAttributes();
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const flashingDisabled = this.getSessionAttribute('flashingDisabled');

        let response = super.createResponse(handlerInput)
            .addDirective(GadgetController.stopButtonDownAnimation())
            .addDirective(GadgetController.stopButtonUpAnimation())
            .addDirective(GadgetController.stopDefaultAnimation())
            .withShouldEndSession(false);

        if (!flashingDisabled) {
            response = response
                .addDirective(GadgetController.createButtonDownAnimation(undefined, 'FF0000'))
                .addDirective(GadgetController.createButtonUpAnimation(undefined, 'FF0000'))
                .addDirective(GadgetController.createHelpAnimation());
        }

        return response;
    }
}
