import { HelpIntentHandler } from "../classes/HelpIntentHandler.class";
import { STATES } from "../../data/states";

export class LaunchHelpHandler extends HelpIntentHandler {
    constructor() {
        super(STATES.Launch);
    }
}