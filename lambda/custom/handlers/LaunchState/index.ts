import { LaunchYesHandler } from "./LaunchYesHandler";
import { LaunchNoHandler } from "./LaunchNoHandler";
import { LaunchHelpHandler } from "./LaunchHelpHandler";

export const LaunchStateRequestHandlers = [
    new LaunchYesHandler(),
    new LaunchNoHandler(),
    new LaunchHelpHandler()
];
