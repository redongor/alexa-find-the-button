import { NoIntentHandler } from "../classes/NoIntentHandler.class";
import { STATES } from "../../data/states";
import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { GameEngine, GadgetController } from "../../utils";
import { ddb } from "../../utils/DynamoDB";

export class LaunchNoHandler extends NoIntentHandler {
    constructor() {
        super(STATES.Launch, STATES.RollCall);
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.setSessionAttribute('inputHandlerId', handlerInput.requestEnvelope.request.requestId);
        super.updateSessionAttributes(handlerInput);
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const flashingDisabled = this.getSessionAttribute('flashingDisabled');

        let response = super.createResponse(handlerInput)
            .addDirective(GameEngine.startInputHandler(30))
            //@ts-ignore
            .withShouldEndSession(undefined);

        if (!flashingDisabled) {
            response = response
                .addDirective(GadgetController.createButtonDownAnimation())
                .addDirective(GadgetController.createButtonUpAnimation())
                .addDirective(GadgetController.createRollCallAnimation());
        }

        return response;
    }
}