import { NoIntentHandler } from "../classes/NoIntentHandler.class";
import { STATES } from "../../data/states";
import { HandlerInput } from "ask-sdk";

export class HideButtonNoHandler extends NoIntentHandler {
    constructor() {
        super(STATES.HideButton);
    }

    public canHandle(handlerInput: HandlerInput) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        return super.canHandle(handlerInput)
            && (sessionAttributes.hasBeenAskedAQuestion || sessionAttributes.hasTimedOut);
    }
}