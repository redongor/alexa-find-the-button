import { HelpIntentHandler } from "../classes/HelpIntentHandler.class";
import { STATES } from "../../data/states";

export class HideButtonHelpHandler extends HelpIntentHandler {
    constructor() {
        super(STATES.HideButton);
    }
}
