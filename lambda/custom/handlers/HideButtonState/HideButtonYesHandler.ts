import { YesIntentHandler } from "../classes/YesIntentHandler.class";
import { STATES } from "../../data/states";
import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { GameEngine, GadgetController } from "../../utils";

export class HideButtonYesHandler extends YesIntentHandler {
    constructor() {
        super(STATES.HideButton);
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.setSessionAttribute('inputHandlerId', handlerInput.requestEnvelope.request.requestId);
        this.setSessionAttribute('handlerStartTime', Date.now());
        super.updateSessionAttributes(handlerInput);
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const buttonId = this.getSessionAttribute('buttonId');
        const flashingDisabled = this.getSessionAttribute('flashingDisabled');

        let response = super.createResponse(handlerInput)
            .addDirective(GameEngine.startInputHandler(30, buttonId))
            .addDirective(GadgetController.stopButtonDownAnimation())
            .addDirective(GadgetController.stopButtonUpAnimation())
            .addDirective(GadgetController.stopDefaultAnimation());

        if (!flashingDisabled) {
            response = response
                .addDirective(GadgetController.createButtonDownAnimation(buttonId))
                .addDirective(GadgetController.createButtonUpAnimation(buttonId))
                .addDirective(GadgetController.createHideButtonAnimation(buttonId));
        }

        return response;
    }
}
