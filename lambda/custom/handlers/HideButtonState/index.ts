import { ButtonHiddenEventHandler } from "./ButtonHiddenEventHandler";
import { HideButtonHelpHandler } from "./HideButtonHelpHandler";
import { HideButtonYesHandler } from "./HideButtonYesHandler";
import { HideButtonNoHandler } from "./HideButtonNoHandler";
import { HideButtonTimedOutEventHandler } from "./HideButtonTimedOutEventHandler";

export const HideButtonStateRequestHandlers = [
    new ButtonHiddenEventHandler(),
    new HideButtonHelpHandler(),
    new HideButtonYesHandler(),
    new HideButtonNoHandler(),
    new HideButtonTimedOutEventHandler()
];
