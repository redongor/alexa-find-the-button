import { ButtonFoundEventHandler } from "./ButtonFoundEventHandler";
import { FindButtonTimedOutEventHandler } from "./FindButtonTimedOutEventHandler";
import { FindButtonHelpHandler } from "./FindButtonHelpHandler";
import { FindButtonYesHandler } from "./FindButtonYesHandler";
import { FindButtonNoHandler } from "./FindButtonNoHandler";

export const FindButtonStateRequestHandlers = [
    new ButtonFoundEventHandler(),
    new FindButtonTimedOutEventHandler(),
    new FindButtonHelpHandler(),
    new FindButtonYesHandler(),
    new FindButtonNoHandler()
];
