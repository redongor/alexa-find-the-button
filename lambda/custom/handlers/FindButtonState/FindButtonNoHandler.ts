import { NoIntentHandler } from "../classes/NoIntentHandler.class";
import { STATES } from "../../data/states";
import { HandlerInput, ResponseBuilder } from "ask-sdk";

export class FindButtonNoHandler extends NoIntentHandler {
    constructor() {
        super(STATES.FindButton);
    }

    public canHandle(handlerInput: HandlerInput) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        return super.canHandle(handlerInput)
            && (sessionAttributes.hasBeenAskedAQuestion || sessionAttributes.hasTimedOut);
    }
}
