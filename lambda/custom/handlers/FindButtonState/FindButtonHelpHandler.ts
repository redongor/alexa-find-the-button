import { HelpIntentHandler } from "../classes/HelpIntentHandler.class";
import { STATES } from "../../data/states";

export class FindButtonHelpHandler extends HelpIntentHandler {
    constructor() {
        super(STATES.FindButton);
    }
}
