import { InputEventHandler } from "../classes/InputEventHandler.class";
import { STATES } from "../../data/states";
import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { GameEngine, GadgetController } from "../../utils";

export class ButtonFoundEventHandler extends InputEventHandler {
    constructor() {
        super('buttonUp', STATES.FindButton, STATES.GameOver);
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.updateHandlerId();
        this.setSessionAttribute('handlerStartTime', Date.now());
        super.updateSessionAttributes(handlerInput); //todo remove all returns from updatesessionattribute calls
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const buttonId = this.getSessionAttribute('buttonId');
        const flashingDisabled = this.getSessionAttribute('flashingDisabled');

        let response = super.createResponse(handlerInput)
            .addDirective(GameEngine.startInputHandler(70, buttonId))
            .addDirective(GadgetController.stopButtonDownAnimation())
            .addDirective(GadgetController.stopButtonUpAnimation())
            .addDirective(GadgetController.stopDefaultAnimation());

        if (!flashingDisabled) {
            response = response
                .addDirective(GadgetController.createButtonDownAnimation(buttonId))
                .addDirective(GadgetController.createButtonUpAnimation(buttonId))
                .addDirective(GadgetController.createGameOverAnimation(buttonId));
        }

        return response;
    }
}
