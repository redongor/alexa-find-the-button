import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { MyRequestHandler } from "./MyRequestHandler.class";
import { STATES } from "../../data/states";

export class NoIntentHandler extends MyRequestHandler {
    constructor(private state: string, private toState?: string) {
        super();
    }

    public canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

        return (sessionAttributes.state === this.state
                || this.state === STATES.Generic)
            && sessionAttributes.hasBeenAskedAQuestion
            && request.type === 'IntentRequest'
            && request.intent.name === 'AMAZON.NoIntent';
    }

    protected performDefaultUpdates() {
        if (this.toState) {
            this.updateState(this.toState);
        }
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.removeSessionAttribute('hasBeenAskedAQuestion');
        return super.updateSessionAttributes(handlerInput);
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const messages = this.getRequestAttribute('messages');;
        return handlerInput.responseBuilder
            .speak(messages[this.state].No())
            .withShouldEndSession(true);
    }
}
