import { RequestHandler, HandlerInput, ResponseBuilder } from "ask-sdk";
import { Response } from "ask-sdk-model";

export class MyRequestHandler implements RequestHandler {
    private setSessionAttributes: any;
    private sessionAttributes: any;
    private requestAttributes: any;

    constructor() { }

    public canHandle(handlerInput: HandlerInput): boolean {
        return true;
    }

    public handle(handlerInput: HandlerInput): Response {
        this.initializeAttributeHandling(handlerInput);
        this.performDefaultUpdates();
        this.updateSessionAttributes(handlerInput);
        return this.createResponse(handlerInput).getResponse();
    }

    protected initializeAttributeHandling(handlerInput: HandlerInput) {
        this.setSessionAttributes = handlerInput.attributesManager.setSessionAttributes;
        this.sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        this.requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    }

    protected performDefaultUpdates() {}

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.saveSessionAttributes();
    }

    protected setSessionAttribute(name: string, value: any) {
        this.sessionAttributes[name] = value;
        this.saveSessionAttributes();
    }

    protected getSessionAttribute(name: string) {
        return this.sessionAttributes[name];
    }

    protected removeSessionAttribute(name: string) {
        delete this.sessionAttributes[name];
        this.saveSessionAttributes();
    }

    protected saveSessionAttributes() {
        this.setSessionAttributes(this.sessionAttributes);
    }

    protected getRequestAttribute(name: string) {
        return this.requestAttributes[name];
    }

    protected updateState(state: string) {
        this.setSessionAttribute('state', state);
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        return handlerInput.responseBuilder;
    }
}
