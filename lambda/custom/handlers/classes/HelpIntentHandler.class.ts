import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { MyRequestHandler } from "./MyRequestHandler.class";
import { GadgetController } from "../../utils";

export class HelpIntentHandler extends MyRequestHandler {
    constructor(private state: string) {
        super();
    }

    public canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        const state = handlerInput.attributesManager.getSessionAttributes().state;

        return state === this.state
            && request.type === 'IntentRequest'
            && request.intent.name === 'AMAZON.HelpIntent';
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.setSessionAttribute('hasBeenAskedAQuestion', true);
        this.removeSessionAttribute('inputHandlerId');
        this.removeSessionAttribute('handlerStartTime');
        return super.updateSessionAttributes(handlerInput);
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const messages = this.getRequestAttribute('messages');;
        const flashingDisabled = this.getSessionAttribute('flashingDisabled');
        const buttonId = this.getSessionAttribute('buttonId');

        let response = handlerInput.responseBuilder
            .speak(messages[this.state].Help())
            .addDirective(GadgetController.stopButtonDownAnimation())
            .addDirective(GadgetController.stopButtonUpAnimation())
            .addDirective(GadgetController.stopDefaultAnimation())
            .withShouldEndSession(false);

        if (!flashingDisabled) {
            response = response
                .addDirective(GadgetController.createButtonDownAnimation(undefined, 'FF0000'))
                .addDirective(GadgetController.createButtonUpAnimation(undefined, 'FF0000'))
                .addDirective(GadgetController.createHelpAnimation(buttonId));
        }

        return response;
    }
}
