import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { GameEngine } from '../../utils';
import { MyRequestHandler } from "./MyRequestHandler.class";
import { Response } from "ask-sdk-model";
import { STATES } from "../../data/states";

export class InputEventHandler extends MyRequestHandler {
    private requestId = '';

    constructor(private eventName: string, //todo re order these params to be consistent with the other handlers
                private state: string,
                private toState?: string) {
        super();
    }

    public canHandle(handlerInput: HandlerInput): boolean {
        const request: any = handlerInput.requestEnvelope.request;
        const attributes = handlerInput.attributesManager.getSessionAttributes();

        return (attributes.state === this.state || this.state === STATES.Generic)
            && GameEngine.matchesEvent(request, attributes.inputHandlerId, this.eventName);
    }

    public handle(handlerInput: HandlerInput): Response {
        this.requestId = handlerInput.requestEnvelope.request.requestId;
        return super.handle(handlerInput);
    }

    protected performDefaultUpdates() {
        if (this.toState) {
            this.updateState(this.toState);
        }
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.removeSessionAttribute('hasBeenAskedAQuestion'); //todo this ain't the right scope for this!
        return super.updateSessionAttributes(handlerInput);
    }

    protected updateHandlerId() {
        this.setSessionAttribute('inputHandlerId', this.requestId);
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const messages = this.getRequestAttribute('messages');;
        return handlerInput.responseBuilder
            .speak(messages[this.state][this.eventName]())
            //@ts-ignore
            .withShouldEndSession(undefined); //todo test whether or not this is having any effect
    }
}
