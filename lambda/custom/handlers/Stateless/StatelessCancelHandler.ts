import { RequestHandler, HandlerInput } from "ask-sdk";
import { Response } from "ask-sdk-model";

export class StatelessCancelHandler implements RequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'AMAZON.CancelIntent';
    }

    handle(handlerInput: HandlerInput): Response {
        const messages = handlerInput.attributesManager.getRequestAttributes().messages;

        return handlerInput.responseBuilder
            .speak(messages.Generic.Cancel())
            .withShouldEndSession(true)
            .getResponse();
    }
}
