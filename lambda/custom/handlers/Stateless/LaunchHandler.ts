import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { STATES } from "../../data/states";
import { MyRequestHandler } from "../classes/MyRequestHandler.class";
import { GameEngine, GadgetController } from "../../utils";

export class LaunchHandler extends MyRequestHandler {
    constructor() {
        super();
    }
    canHandle(handlerInput: HandlerInput): boolean {
        const request: any = handlerInput.requestEnvelope.request;
        return request.type === 'LaunchRequest'
            || request.type === 'NewSession';
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        const instructionsEnabled = this.getSessionAttribute('instructionsEnabled');
        if (instructionsEnabled) {
            this.updateState(STATES.Launch);
            this.setSessionAttribute('hasBeenAskedAQuestion', true);
        } else {
            this.updateState(STATES.RollCall);
            this.setSessionAttribute('inputHandlerId', handlerInput.requestEnvelope.request.requestId);
        }
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const messages = this.getRequestAttribute('messages');
        const instructionsEnabled = this.getSessionAttribute('instructionsEnabled');

        if (instructionsEnabled) {
            return handlerInput.responseBuilder.speak(messages.Generic.Launch(true))
                .withShouldEndSession(false);
        } else {
            const flashingDisabled = this.getSessionAttribute('flashingDisabled');

            let response = super.createResponse(handlerInput)
                .speak(messages[STATES.Launch].No())
                .addDirective(GameEngine.startInputHandler(30))
                //@ts-ignore
                .withShouldEndSession(undefined);

            if (!flashingDisabled) {
                response = response
                    .addDirective(GadgetController.createButtonDownAnimation())
                    .addDirective(GadgetController.createButtonUpAnimation())
                    .addDirective(GadgetController.createRollCallAnimation());
            }

            return response;
        }
    }
}
