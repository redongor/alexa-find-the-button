import { InputEventHandler } from "../classes/InputEventHandler.class";
import { STATES } from "../../data/states";
import { ResponseBuilder, HandlerInput } from "ask-sdk";
import { GadgetController } from "../../utils";

export class StopAnimationHandler extends InputEventHandler {
    private hasBeenAskedAQuestion = false;

    constructor() {
        super('buttonDown', STATES.Generic);
    }

    protected initializeAttributeHandling(handlerInput: HandlerInput) {
        super.initializeAttributeHandling(handlerInput);
        this.hasBeenAskedAQuestion = this.getSessionAttribute('hasBeenAskedAQuestion');
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        let response = handlerInput.responseBuilder
            //@ts-ignore
            .withShouldEndSession(undefined);

        if (this.shouldStopAnimation()) {
            response = response
                .addDirective(GadgetController.stopDefaultAnimation());
        }

        return response;
    }

    private shouldStopAnimation(): boolean {
        const state = this.getSessionAttribute('state');

        return !this.hasBeenAskedAQuestion && (
            state === STATES.HideButton
            || state === STATES.FindButton
            || state === STATES.RollCall);
    }
}
