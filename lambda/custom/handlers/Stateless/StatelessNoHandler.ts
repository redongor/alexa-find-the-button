import { NoIntentHandler } from "../classes/NoIntentHandler.class";
import { STATES } from "../../data/states";

export class StatelessNoHandler extends NoIntentHandler {
    constructor() {
        super(STATES.Generic);
    }
}
