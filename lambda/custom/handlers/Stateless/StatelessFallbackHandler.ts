import { MyRequestHandler } from "../classes/MyRequestHandler.class";
import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { STATES } from "../../data/states";

export class StatelessFallbackHandler extends MyRequestHandler {
    public canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'AMAZON.FallbackIntent';
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const hasBeenAskedAQuestion = this.getSessionAttribute('hasBeenAskedAQuestion');
        const state = this.getSessionAttribute('state');
        let speechResponse = '';

        if (!hasBeenAskedAQuestion) {
            if (state === STATES.FindButton) {
                speechResponse = `<audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_32s_full_01'/>`;
            }
            else if (state === STATES.HideButton) {
                speechResponse = `<audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_64s_minimal_01'/>`;
            }
            else if (state === STATES.RollCall) {
                speechResponse = `
                    <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_waiting_loop_30s_01'/>
                    <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_waiting_loop_30s_01'/>`;
            }
        }

        return  super.createResponse(handlerInput)
            .speak(speechResponse)
            //@ts-ignore
            .withShouldEndSession(undefined);
    }
}