import { HandlerInput, ErrorHandler } from 'ask-sdk';
import { Response } from 'ask-sdk-model';
import { STATES } from '../../data/states';

export class StatelessErrorHandler implements ErrorHandler {
    canHandle(): boolean {
        return true;
    }

    handle(handlerInput: HandlerInput, error: Error): Response {
        console.error(`Error: ${error}`);

        const request: any = handlerInput.requestEnvelope.request;
        const intentType = request.type;
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        const state = sessionAttributes.state;
        const hasBeenAskedAQuestion = sessionAttributes.hasBeenAskedAQuestion;
        let speechResponse = handlerInput.attributesManager.getRequestAttributes().messages.Generic.Error();

        if (intentType === 'GameEngine.InputHandlerEvent'
            || (intentType === 'IntentRequest'
                && request.intent.name.indexOf('AMAZON') !== -1)
         )  {
             speechResponse = ``;
             if (!hasBeenAskedAQuestion) {
                if (state === STATES.FindButton) {
                    speechResponse = `<audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_32s_full_01'/>`;
                }
                else if (state === STATES.HideButton) {
                    speechResponse = `<audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_64s_minimal_01'/>`;
                }
                else if (state === STATES.RollCall) {
                    speechResponse = `
                        <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_waiting_loop_30s_01'/>
                        <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_waiting_loop_30s_01'/>`;
                }
            }
        }


        return handlerInput.responseBuilder
            .speak(speechResponse)
            .getResponse();
    }
}
