import { RequestHandler, HandlerInput } from "ask-sdk";
import { Response } from "ask-sdk-model";
import { GadgetController } from "../../utils";

export class SessionEndedHandler implements RequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    }

    handle(handlerInput: HandlerInput): Response {
        const messages = handlerInput.attributesManager.getRequestAttributes().messages;

        return handlerInput.responseBuilder
            .speak(messages.stop)
            .addDirective(GadgetController.stopDefaultAnimation())
            .addDirective(GadgetController.stopButtonDownAnimation())
            .addDirective(GadgetController.stopButtonUpAnimation())
            .getResponse();
    }
}
