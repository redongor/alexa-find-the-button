import { RequestHandler, HandlerInput } from "ask-sdk";
import { Response } from "ask-sdk-model";

export class StatelessStopHandler implements RequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'AMAZON.StopIntent';
    }

    handle(handlerInput: HandlerInput): Response {
        const messages = handlerInput.attributesManager.getRequestAttributes().messages;

        return handlerInput.responseBuilder
            .speak(messages.Generic.Exit())
            .withShouldEndSession(true)
            .getResponse();
    }
}