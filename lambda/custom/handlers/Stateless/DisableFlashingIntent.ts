import { MyRequestHandler } from "../classes/MyRequestHandler.class";
import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { GadgetController } from "../../utils";
import { STATES } from "../../data/states";

export class DisableFlashingIntent extends MyRequestHandler {
    public canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'DisableFlashingIntent';
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        this.setSessionAttribute('flashingDisabled', true);
        super.updateSessionAttributes(handlerInput);
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const messages = this.getRequestAttribute('messages');
        const state = this.getSessionAttribute('state');

        let response = handlerInput.responseBuilder
            .speak(messages[state].DisableFlashing())
            .addDirective(GadgetController.stopDefaultAnimation())
            .addDirective(GadgetController.stopButtonDownAnimation());

        if (state === STATES.FindButton
            || state === STATES.HideButton
            || state === STATES.RollCall) {
            //@ts-ignore
            response = response.withShouldEndSession(undefined)
        }
        else {
            response = response.withShouldEndSession(false);
        }

        return response;
    }
}
