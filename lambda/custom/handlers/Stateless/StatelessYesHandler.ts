import { YesIntentHandler } from "../classes/YesIntentHandler.class";
import { STATES } from "../../data/states";

export class StatelessYeshandler extends YesIntentHandler {
    constructor() {
        super(STATES.Generic);
    }
}
