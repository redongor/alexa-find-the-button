import { MyRequestHandler } from "../classes/MyRequestHandler.class";
import { HandlerInput, ResponseBuilder } from "ask-sdk";
import { STATES } from "../../data/states";

export class TimeRemainingHandler extends MyRequestHandler {
    public canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
        && request.intent.name === 'TimeRemainingIntent';
    }

    protected updateSessionAttributes(handlerInput: HandlerInput) {
        const state = this.getSessionAttribute('state');
        if ([STATES.FindButton, STATES.HideButton].indexOf(state) === -1) {
            this.setSessionAttribute('hasBeenAskedAQuestion', true);
        }
        super.updateSessionAttributes(handlerInput);
    }

    protected createResponse(handlerInput: HandlerInput): ResponseBuilder {
        const state = this.getSessionAttribute('state');
        if (state === STATES.FindButton || state === STATES.HideButton) {
            const messages = this.getRequestAttribute('messages');
            const secondsRemaining = this.getTimeRemaining();
            return handlerInput.responseBuilder
                .speak(messages.Generic.TimeRemaining(secondsRemaining, state))
                //@ts-ignore
                .withShouldEndSession(undefined);
        }
        else {
            return handlerInput.responseBuilder
                .speak('There is no timer running right now. Do you want to keep playing?')
                .withShouldEndSession(false);
        }
    }

    private getTimeRemaining(): number {
        const now: number = Date.now();
        const startTime: number = this.getSessionAttribute('handlerStartTime');

        return 30 - parseInt(((now - startTime) / 1000).toString());
    }
}
