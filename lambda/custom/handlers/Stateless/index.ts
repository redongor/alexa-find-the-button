import { LaunchHandler } from './LaunchHandler';
import { StatelessHelpHandler } from './StatelessHelpHandler';
import { StatelessYeshandler } from './StatelessYesHandler';
import { StatelessNoHandler } from './StatelessNoHandler';
import { StatelessStopHandler } from './StatelessStopHandler';
import { StatelessCancelHandler } from './StatelessCancelHandler'
import { SessionEndedHandler } from './SessionEndedHandler';
import { StatelessFallbackHandler } from './StatelessFallbackHandler';

import { DisableFlashingIntent } from './DisableFlashingIntent';
import { EnableFlashingIntent } from './EnableFlashingIntent';
import { StopAnimationHandler } from './StopAnimationHandler';
import { TimeRemainingHandler } from './TimeRemainingHandler';

import { StatelessErrorHandler } from './StatelessErrorHandler';

export const GenericRequestHandlers = [
    new LaunchHandler(),
    new StatelessHelpHandler(),
    new StatelessYeshandler(),
    new StatelessNoHandler(),
    new StatelessStopHandler(),
    new StatelessCancelHandler(),
    new SessionEndedHandler(),
    new DisableFlashingIntent(),
    new EnableFlashingIntent(),
    new StopAnimationHandler(),
    new TimeRemainingHandler(),
    new StatelessFallbackHandler()
];

export const GenericErrorHandlers = [
    new StatelessErrorHandler()
];
