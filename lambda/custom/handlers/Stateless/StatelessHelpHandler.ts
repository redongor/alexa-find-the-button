import { HelpIntentHandler } from "../classes/HelpIntentHandler.class";
import { STATES } from "../../data/states";

export class StatelessHelpHandler extends HelpIntentHandler {
    constructor() {
        super(STATES.Generic);
    }
}
