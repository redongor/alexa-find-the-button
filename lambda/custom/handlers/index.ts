import { RollCallStateRequestHandlers } from './RollCallState';
import { GenericRequestHandlers, GenericErrorHandlers } from './Stateless';
import { HideButtonStateRequestHandlers } from './HideButtonState';
import { FindButtonStateRequestHandlers } from './FindButtonState';
import { GameOverStateRequestHandlers } from './GameOverState';
import { LaunchStateRequestHandlers } from './LaunchState';

export const RequestHandlers = [
    ...LaunchStateRequestHandlers,
    ...RollCallStateRequestHandlers,
    ...HideButtonStateRequestHandlers,
    ...FindButtonStateRequestHandlers,
    ...GameOverStateRequestHandlers,
    ...GenericRequestHandlers // todo this has to be at the end due to the InputEventHandler class' canhandle method
];

export const ErrorHandlers = [
    ...GenericErrorHandlers
];
