import { RequestInterceptor, HandlerInput } from "ask-sdk";

export class LogRequestInterceptor implements RequestInterceptor {
    process(handlerInput: HandlerInput): Promise<void> {
        return new Promise<void>((resolve) => {
            const request: any = handlerInput.requestEnvelope.request;
            const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
            console.log('------------------------- Begin Request Loop -------------------------');
            console.log(` -- Request Type: ${request.type}`);
            if (request.type === 'GameEngine.InputHandlerEvent') {
                console.log(` ---- Event Name(s): ${request.events.map((event: any) => event.name).join(', ')}`);
            }
            console.log(` -- Request ID: ${request.requestId}`);
            if (request.intent) {
                console.log(` -- Intent Name: ${request.intent.name}`);
                console.log(` -- Intent Confirmation Status: ${request.intent.confirmationStatus}`);
            }
            console.log(` -- Request Body:\n${JSON.stringify(handlerInput.requestEnvelope, null, 4)}`);
            console.log(` -- Session Attributes:\n${JSON.stringify(sessionAttributes, null, 4)}`);
            console.log(` -- State: ${sessionAttributes.state}`);
            resolve();
        });
    }
}
