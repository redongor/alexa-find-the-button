import { RequestInterceptor, HandlerInput } from 'ask-sdk';
import { ddb } from '../utils/DynamoDB';
import { User } from '../data-models/User';
import { STATES } from '../data/states';

export class UserInterceptor implements RequestInterceptor {
    process(handlerInput: HandlerInput): Promise<void> {
        return new Promise<void>(resolve => {
            const attributes = handlerInput.attributesManager.getSessionAttributes();
            //@ts-ignore
            const userId = handlerInput.requestEnvelope.session.user.userId;
            if (attributes.instructionsEnabled === false) {
                resolve();
            } else {
                if (attributes.instructionsEnabled === true) {
                    console.log('dumb user');
                    //@ts-ignore
                    if (attributes.state === STATES.Launch
                            && attributes.hasBeenAskedAQuestion
                            && handlerInput.requestEnvelope.request.type === 'IntentRequest'
                            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.NoIntent') {
                        console.log('boutta update this fool');
                        attributes.instructionsEnabled = false;
                        handlerInput.attributesManager.setSessionAttributes(attributes);
                        ddb.getUser(userId, (user: User) => {
                            console.log('userinterceptor -> got user: ', user);
                            user.apps['find-the-button'].instructionsEnabled = false;
                            ddb.updateUser(user, () => {
                                console.log('userinterceptor -> updated user');
                                resolve();
                            });
                        });
                    } else {
                        console.log('get yeeted on');
                        resolve();
                    }
                } else {
                    console.log('new user!');
                    if (userId) {
                        ddb.getUser(userId, (user: User) => {
                            if (user && user.apps && user.apps['find-the-button']) {
                                attributes.instructionsEnabled = user.apps['find-the-button'].instructionsEnabled;
                                resolve();
                            } else {
                                if (!user) {
                                    user = {
                                        userId: userId,
                                        apps: {
                                            'find-the-button': {
                                                instructionsEnabled: true
                                            }
                                        }
                                    };
                                } else if (!user.apps) {
                                    user.apps = {};
                                }
                                user.userId = userId;
                                user.apps['find-the-button'] = {
                                    instructionsEnabled: true
                                };
                                attributes.instructionsEnabled = true;
                                handlerInput.attributesManager.setSessionAttributes(attributes);
                                ddb.updateUser(user, () => {
                                    resolve();
                                });
                            }
                        });
                    } else {
                        resolve();
                    }
                }
            }
        });
    }
}
