import { MessagesInterceptor } from './MessagesInterceptor';
import { LogRequestInterceptor } from './LogRequestInterceptor';
import { UserInterceptor } from './UserInterceptor';

export const RequestInterceptors = [
    new LogRequestInterceptor(),
    new MessagesInterceptor(),
    new UserInterceptor()
];
