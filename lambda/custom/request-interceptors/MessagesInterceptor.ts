import { RequestInterceptor, HandlerInput } from "ask-sdk";
import { MESSAGES } from "../data/messages";

export class MessagesInterceptor implements RequestInterceptor {
    process(handlerInput: HandlerInput): Promise<void> {
        return new Promise<void>((resolve) => {
            handlerInput.attributesManager.setRequestAttributes({messages: MESSAGES.English});
            resolve();
        });
    }
}
