import { SkillBuilders } from 'ask-sdk';

import { RequestHandlers, ErrorHandlers } from './handlers';
import { RequestInterceptors } from './request-interceptors';
import { ResponseInterceptors } from './response-interceptors';

const skillBuilder = SkillBuilders.custom();

export const handler = skillBuilder
    .addRequestInterceptors(...RequestInterceptors)
    .addRequestHandlers(...RequestHandlers)
    .addErrorHandlers(...ErrorHandlers)
    .addResponseInterceptors(...ResponseInterceptors)
    .lambda();
