import { STATES } from "../states";

const FindButton = {
    buttonUp: ()=> {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_positive_response_01'/>
            <say-as interpret-as="interjection">woo hoo</say-as>, <prosody rate="110%">you found the <prosody volume="loud">button!</prosody></prosody>
            If you wanna play again, just press it one more time!`;
    },
    timedOut: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_negative_response_02'/>
            Time's up! The button must have been hidden really well!
            Want to play again?`;
    },
    Help: () => {
        return `
            You need to find and press the button before time is up!
            Wanna keep going?`;
    },
    Yes: () => {
        return `
            Alright! Good luck!
            As the timer runs <prosody pitch="+15%">down,</prosody>
            the button'll flash, red and orange.
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_32s_full_01'/>`;
    },
    No: () => {
        return `
            Thanks <prosody rate="fast">for</prosody> <prosody volume="loud">playing!</prosody>
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_outro_01'/>`;
    },
    DisableFlashing: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_02'/>
            Button lights disabled. Keep finding the button!`;
    },
    EnableFlashing: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_02'/>
            Button lights enabled. Keep finding the button!`;
    }
};

const GameOver = {
    Yes: () => {
        return `
            Alright, awesome!
            Press the button when you're ready to start hiding.
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_waiting_loop_30s_01'/>
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_waiting_loop_30s_01'/>`;
    },
    No: () => {
        return `
            Thanks <prosody rate="fast">for</prosody> <prosody volume="loud">playing!</prosody>
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_outro_01'/>`;
    },
    Help: () => {
        return `
            Right now I just need to know if you want to <prosody rate="90%">play again.</prosody>
            Are you <prosody volume="loud" rate="70%" pitch="-10%"><emphasis level="reduced">game</emphasis>?</prosody>`;
    },
    buttonUp: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_positive_response_01'/>
            Hide it in the best spot you can find!
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_64s_minimal_01'/>`;
    },
    timedOut: () => {
        return `Do you wanna keep playing?`;
    },
    DisableFlashing: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_02'/>
            Button lights disabled. Want to play again?`;
    },
    EnableFlashing: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_02'/>
            Button lights enabled. Want to play again?`;
    }
}

const Generic = {
    Cancel: () => 'Cancelling',
    Error: () => {
        return `
            <s><say-as interpret-as="interjection">uh oh</say-as>, Something went wrong.</s>
            <s>Please try again!</s>`;
    },
    Exit: () => {
        return `
            <s>Let's <emphasis>playagain</emphasis> <prosody volume="x-loud">soon!</prosody></s>
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_outro_01'/>`;
    },
    Yes: () => '',
    No: () => '',
    Launch: (instructionsEnabled: boolean) => {
        if (instructionsEnabled) {
            return `
                <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_bridge_02'/>
                <s>Welcome to <emphasis>Find the Button!</emphasis></s>
                Want me to explain how to play?`;
        } else {
            return RollCall.Yes();
        }
    },
    TimeRemaining: (secondsRemaining: number, state: string) => {
        let result = '';

        if (secondsRemaining <= 10) {
            if (secondsRemaining <= 5) {
                result = `
                    <s><prosody rate="fast">Only<emphasis level="strong"> ${secondsRemaining} </emphasis><prosody rate="medium">second${secondsRemaining === 1 ? 's' : ''}</prosody></prosody></s>`;
            }
            result = `
                <s><prosody rate="fast">Only<emphasis level="strong"> ${secondsRemaining} </emphasis><prosody rate="medium">seconds left</prosody></prosody></s>`;
        }
        else {
            result = `
                <s><prosody rate="fast">You've got about<emphasis> ${secondsRemaining} </emphasis><prosody rate="medium">seconds left</prosody></prosody></s>`;
        }

        if (state === STATES.FindButton) {
             result += `
                <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_32s_full_01'/>`;
        }
        else if (state === STATES.HideButton) {
            result += `
                <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_64s_minimal_01'/>`;
        }

        return result;
    }
};

const HideButton = {
    buttonUp: () => {
        return `
        <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_positive_response_01'/>
        The button is <prosody volume="x-loud">hidden!</prosody>
        Try to find it in the next 30 seconds!
        <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_32s_full_01'/>`;
    },
    timedOut: () => {
        return `
        <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_negative_response_01'/>
            <say-as interpret-as="interjection">uh oh</say-as>, time's up!
            <s>Wanna <prosody rate="fast">keep</prosody> going?</s>`;
    },
    Yes: () => {
        return `
            Alright!
            Go hide the button!
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_64s_minimal_01'/>`;
    },
    No: () => {
        return `
            Thanks for <emphasis level="reduced">playing!</emphasis>
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_outro_01'/>`;
    },
    Help: () => {
        return `
        Right now you should be hiding the button.
        When you've found a good spot, press it!
        Wanna keep going?`;
    },
    DisableFlashing: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_02'/>
            Button lights disabled. Keep hiding the button!
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_64s_minimal_01'/>`;
    },
    EnableFlashing: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_02'/>
            Button lights enabled. Keep hiding the button!
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_64s_minimal_01'/>`;
    }
};

const Launch = {
    Yes: () => {
        return `
            Ok! This game requires only one echo button and at least two players.
            The players will take turns hiding and finding the button.
            You have thirty seconds to do each.
            The buttons will light up and flash at different times, though you can disable this at any time by saying 'disable the button lights'.
            Need me to repeat that?`;
    },
    No: () => {
        return `
            Alright!
            <s>To get started, press the button you want to play with.</s>
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_waiting_loop_30s_01'/>
            Press the button you want to hide.
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_waiting_loop_30s_01'/>`;
    },
    Help: () => {
        return `
            This game requires only one echo button and at least two players.
            The players will take turns hiding and finding the button.
            You have thirty seconds to hide the button and thirty seconds to find it.
            When you find a good hiding spot or when you find the button press it to let me know!
            The buttons will light up and flash at different times, though you can disable this at any time by saying 'disable the button lights'.
            Need me to repeat that? `;
    },
    DisableFlashing: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_02'/>
            Button lights disabled. Need to hear instructions on how to play?`;
    },
    EnableFlashing: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_02'/>
            Button lights enabled. Need to hear instructions on how to play?`;
    }
};

const RollCall = {
    buttonUp: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_01'/>
            Ok, now that you've picked the button, it's time to go hide it!
            Be sure to press it when you find a good spot to let me know that you hid it!
            You've got thirty seconds!
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_countdown_loop_64s_minimal_01'/>`;
    },
    timedOut: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_02'/>
            Do you still want to play?`;
    },
    No: () => {
        return `
            <say-as interpret-as="interjection">Goodbye!</say-as>
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_outro_01'/>`;
    },
    Yes: () => {
        return `
            Ok. Press <prosody rate="115%">the button</prosody> you <prosody rate="115%">want to</prosody> play with.
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_waiting_loop_30s_01'/>`;
    },
    Help: () => {
        return `
            Right now, I need you to pick a button and press it.
            You'll then take turns hiding and finding it.
            Wanna keep playing?`;
    },
    DisableFlashing: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_02'/>
            Button lights disabled. Please press the button you want to play with.`;
    },
    EnableFlashing: () => {
        return `
            <audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_02'/>
            Button lights enabled. Please press the button you want to play with.`;
    }
};

export const ENGLISH = {
    FindButton: FindButton,
    GameOver: GameOver,
    Generic: Generic,
    HideButton: HideButton,
    Launch: Launch,
    RollCall: RollCall
};
