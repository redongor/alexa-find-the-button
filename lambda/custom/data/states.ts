export const STATES = {
    Generic: 'Generic',
    Launch: 'Launch',
    RollCall: 'RollCall',
    HideButton: 'HideButton',
    FindButton: 'FindButton',
    GameOver: 'GameOver'
};
