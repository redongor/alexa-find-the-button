import { ResponseInterceptor, HandlerInput } from "ask-sdk";

export class LogResponseInterceptor implements ResponseInterceptor {
    process(handlerInput: HandlerInput): Promise<void> {
        return new Promise<void>((resolve) => {
            const response = handlerInput.responseBuilder.getResponse();
            console.log(` -- Response Body:\n${JSON.stringify(response, null, 4)}`);
            console.log('------------------------- End Request Loop -------------------------');
            resolve();
        });
    }
}
