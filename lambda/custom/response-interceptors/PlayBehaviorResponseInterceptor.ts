import { ResponseInterceptor, HandlerInput } from "ask-sdk";
import { Directive, Response } from "ask-sdk-model";

export class PlayBehaviorResponseInterceptor implements ResponseInterceptor {
    process(handlerInput: HandlerInput): Promise<void> {
        return new Promise<void>((resolve) => {
            const response: any = handlerInput.responseBuilder.getResponse();
            if (response && hasGameEngineDirective(response) && response.outputSpeech) {
                response.outputSpeech.playBehavior = 'REPLACE_ALL';
            }
            resolve();
        });
    }
}

function hasGameEngineDirective(response: Response) {
    return response.directives && response.directives.filter(findGameEngineEvents).length > 0;
}

function findGameEngineEvents(directive: Directive) {
    return directive.type.indexOf('GameEngine') !== -1;
}
