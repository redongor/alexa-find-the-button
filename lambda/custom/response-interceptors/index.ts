import { LogResponseInterceptor } from './LogResponseInterceptor';
import { PlayBehaviorResponseInterceptor } from './PlayBehaviorResponseInterceptor';

export const ResponseInterceptors = [
    new LogResponseInterceptor(),
    new PlayBehaviorResponseInterceptor()
];
